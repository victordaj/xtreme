#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <termios.h>


#include <sys/socket.h> 
#include <arpa/inet.h> 

#include <stdint.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>

#define READ_CHUNK 1 
#define BATCH_SIZE 256

extern int errno ;


char ip[25];
int port;

typedef struct file{
	char *name;
	char type;
} sfl;

typedef struct panel{
	sfl ** sf;
	int len; 
} pan;


void * allocated_buffer_read(int socket, void *buf, int *size){

  *size = 0;
  int bytes_read;
  do{
    buf = (void *)realloc(buf, *size + READ_CHUNK);
    bytes_read = read(socket, buf + *size, READ_CHUNK);
    *size += bytes_read;
  } while (bytes_read == READ_CHUNK);

  buf = (void *)realloc(buf, *size);
  return buf;
}



int make_connection(){
    int sock = 0; 
    struct sockaddr_in serv_addr; 
    

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) { 
        printf("\n Socket creation error \n"); 
        exit(-1);
    } 
   

    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(port); 
       
    if(inet_pton(AF_INET, ip, &serv_addr.sin_addr)<=0) { 
        printf("\nInvalid address/ Address not supported \n"); 
        exit(-1);
    } 
   

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) { 
        printf("\nConnection Failed \n"); 
       	exit(-1);
    } 

    return sock;
}

pan *unpack(void *data){

	sfl ** files = NULL;

	int file_nr =  *(int *)data;
	int offset = 4;

	files = (sfl **)realloc(files, file_nr * sizeof(sfl *));

	int i = 0;

	for (;i<file_nr;i++) {

		files[i] = (sfl *)malloc(sizeof(sfl));
		files[i]->type =  *(char *)(data + offset);

		offset += 1;
		uint16_t len =  *(uint16_t *)(data + offset);
		// printf("%d ", len);
		files[i]->name = malloc(len + 1);
		offset += 2;
		strncpy(files[i]->name, (char *)(data) + offset, len);
		files[i]->name[len] = 0;
		// puts(files[i]->name);
		offset += len;
	}

	pan *pannel = (pan *)malloc(sizeof(pan));

	pannel->sf = files;
	pannel->len = file_nr;

	free(data);
	return pannel;
}


int read_socket_write_to_file(int socket, int block_size, char *file, int total_blocks){

  FILE *fp = fopen(file,"wb");
  int blocks = 0;
  void *block = malloc(block_size);
  int bytes_read;
  while((bytes_read = read(socket, block, block_size)) > 0) {
     fwrite(block, 1, bytes_read, fp);
     blocks++;

  }

  return blocks;
}

char *get_local_file_path(char *remote_file, char *local_dir) {
    char *filename = strrchr(remote_file,'/');

    if (!filename) {
        return NULL;
    }
    filename += 1;
    char *local_file_path = malloc(strlen(local_dir)+strlen(filename) + 1);
    strcpy(local_file_path, local_dir);
    strcat(local_file_path, "/");
    strcat(local_file_path, filename);
    
    return local_file_path;
}

int download(char *remote_file, char *local_dir) {
    
    int socket = make_connection();
    char *request = malloc(strlen(remote_file)+1);
    request[0] = 'd';
    strcpy(request+1, remote_file);

    send(socket , request , strlen(request) , 0 ); 

    int file_size = 0;
    int block_size = 0;
    int blocks = 0;

    read(socket, &file_size, sizeof(int));
    read(socket, &block_size, sizeof(int));

    if (file_size%block_size == 0)
        blocks = file_size/block_size;
    else
        blocks = file_size/block_size + 1;

    send(socket, &blocks, sizeof(int), 0);
        
    char* local_file_path = get_local_file_path(remote_file, local_dir);

    if (!local_file_path)
        return -1;

    int written_blocks = 0;

    written_blocks = read_socket_write_to_file(socket, block_size, local_file_path, blocks);
    printf("%d", written_blocks);
    if (written_blocks != blocks) {
        return -2;
    }
    
    return 0;
}
int upload(char *file_path, char *right_dir_path){
    int socket = make_connection();

    int* file_size;
    int* chunk_size;
    FILE* fp;
    void *cmd;
    char c='u';
    char blocks_c[4];
    int blocks=0;
    void *file_name;
    void *right_file_path;
    
    file_name=strrchr(file_path,'/');
    printf("file name=%s, right_dir path=%s\n",(char *)file_name,right_dir_path);

    right_file_path=(void *)malloc(strlen(right_dir_path)+ strlen((char *)file_name)+1);

    memcpy(right_file_path, right_dir_path, strlen(right_dir_path)+1);
    memcpy(right_file_path+strlen(right_dir_path), file_name, strlen((char *)file_name)+1);

    printf("right file name=%s\n",(char *)right_file_path);

    file_size=(int *)malloc(1);
    chunk_size=(int *)malloc(1);
    *chunk_size=4096;

    cmd=(void *)malloc(1);
    memcpy(cmd,(void *)&c,1);

    fp=fopen(file_path, "rb");
    fseek(fp, 0L, SEEK_END);
    *file_size=ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    cmd=(void *)realloc(cmd,5);
    memcpy(cmd+1, (void *)file_size, 4);

    cmd=(void *)realloc(cmd,9);
    memcpy(cmd+5, (void *)chunk_size, 4);

    cmd=(void *)realloc(cmd,9+strlen(right_dir_path)+ strlen((char *)file_name)+1); 
    memcpy(cmd+9, (void *)right_file_path, strlen((char *)right_file_path)+1);
 
    printf("file size=%d, block size=%d\n",*(int *)(cmd+1),*(int *)(cmd+5));

    send(socket , cmd , 9 + strlen(file_path) + 1 , 0 ); 
    read(socket, blocks_c, 4);
    blocks=*(int *)blocks_c;

    printf("received from server no of blocks=%d\n",blocks);
    
    void *buff;
    int bytes_read=0;


    int i = 0;
    buff=(void *)malloc(*chunk_size);
    for(i=0; i<blocks; i++) {
		bytes_read=fread(buff, 1, *chunk_size, fp);
		send(socket, buff, bytes_read, 0);
    }
    fclose(fp);
    close(socket);
    return 0;
}


int sv_remove(char *file){
	int socket = make_connection();
    
    char cmd[257] = "r";

    strcat(cmd, file);

    send(socket , cmd , strlen(cmd) , 0 ); 
    read(socket, cmd, READ_CHUNK);
    close(socket);
	return 0;

}

int local_remove(char *file){
	return remove(file);
}




pan *server_list_dir(char * dir){

	int socket = make_connection();
    
    char cmd[257] = "f";

    strcat(cmd, dir);

    send(socket , cmd , strlen(cmd) , 0 ); 

   
    void *buffer = NULL; 
    int sz;
    buffer = allocated_buffer_read( socket , buffer, &sz); 

    close(socket);

    return unpack(buffer);
}

int get_key(){
	int c;
		c = getchar();

		if (c == 27) {
			c = getchar();
			if ( c == 91) {
				c = getchar();
					return c - 65;
			}
		}

		return c;

}
void print_pan(pan* pannel){
	for(int i = 0; i < pannel -> len; i++) {
		sfl *files = *(pannel -> sf + i);
		printf("%s : %d\n", files -> name, files -> type);
	}
}

void print_menu() {
	printf("Choose your option by selecting the index number : \n");
	printf("0. Back\n");
	printf("1. Download file\n");
	printf("2. Upload file\n");
	printf("3. List directories from server\n");
	printf("4. Remove file\n");
	printf("5. Exit program\n");
}

int main(int argc, char* argv[]){

	if(argc != 3) {
		printf("Please call: %s <ip> <port>\n", argv[0]);
		exit(1);
    }

    strcpy(ip, argv[1]);
	port = atoi(argv[2]);

	int option;
	int flag = 1;
	char *file_path = malloc(128);
	char *dir_path = malloc(128);

	print_menu();
	

	while(flag) {
	scanf("%d", &option);
	switch(option) {
		case 1:
			printf("List the directories from the server and then choose what you want to download ! \n");
			printf("Put 0 if u want to go back , 1 if u want to continue\n");
			scanf("%d", &option);
			if(option != 0) {
				printf("Please insert the file path :\n");
				scanf("%s", file_path);
				printf("Please insert the path of the local directory\n");
				scanf("%s", dir_path);
				download(file_path, dir_path);
			}
			print_menu();
			break;
		case 2:
			printf("List the directories from the server and then choose where you want to upload\n");
			printf("Put 0 if u want to go back , 2 if u want to continue\n");
			scanf("%d", &option);
			if(option != 0) {
				printf("Please insert the file path that you want to upload: \n ");
				scanf("%s", file_path);
				printf("Please insert the path of the remote directory\n");
				scanf("%s", dir_path);
				download(file_path, dir_path);
			}
			print_menu();
			break;
		case 3:
			printf("Please choose first '/home/victor/Desktop/orchid/Server/docs/' and then what else do you need :\n");
			scanf("%s", dir_path);
			print_pan(server_list_dir(dir_path));
			print_menu();
			break;
		case 4:
			printf("Specify the file path you want to delete\n");
			scanf("%s", file_path);
			sv_remove(file_path);
			print_menu();
			break;
		case 5:
			printf("Exited program\n");
			flag = 0;
		default:
			print_menu();
			break;
	}
	}
}
